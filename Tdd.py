import unittest
from validacpf_Raphael import valida_cpf
from validacpf_Raphael import retira_formatacao


class TestStringMethods(unittest.TestCase):

    def test_formatacao(self):
        self.assertEqual(
            retira_formatacao("IU2323UIU"), (
                "2323"))
        self.assertEqual(
            retira_formatacao("123-321/123"), ("123321123"))
        self.assertEqual(
            retira_formatacao(""), (""))

    def test_valida(self):
        self.assertEqual(
            valida_cpf ("39324331817"),True)
        self.assertEqual(
            valida_cpf(""),False)
        self.assertEqual(
            valida_cpf("09876543212"), False)
        self.assertEqual(
            valida_cpf("11111111111"),False)


def runTests():
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(TestStringMethods)
    unittest.TextTestRunner(verbosity=2, failfast=True).run(suite)

runTests()
